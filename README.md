# TSLint Config for Trans World Radio projects

## Installation

Add the following to your `devDependencies` in your project's `package.json`:

    ...
    "tslint-config-transworldradio": "git+https://git@gitlab.com/transworldradio/tslint-config-transworldradio.git",
    ...

Then use npm to install it

    $ npm install tslint-config-transworldradio

Now base tslint.json should be

    {
        "extends": "tslint-config-transworld-radio"
    }

## License

**ISC**

